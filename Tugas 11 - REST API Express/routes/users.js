var express = require('express');
var router = express.Router();
//controller
const UserController = require('../controllers/users')
/* GET users listing. */

router.post('/login', UserController.login);
// router.get('/:id',UserController.findById)
router.get('/:id',(req,res)=>{
    console.log("parameter id : ",req.params.id)
    res.send(`hello ${req.params.id} user`)
})
router.post('/register', UserController.register)
module.exports = router;
