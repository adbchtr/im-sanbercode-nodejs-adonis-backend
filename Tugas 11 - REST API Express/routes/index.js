var express = require('express');
var router = express.Router();
const UserController = require('../controllers/users')
/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('Hello Express');
});
router.post('/karyawan/:name/siswa', UserController.addStudents)
router.get('/karyawan', UserController.findAll);
router.post('/login', UserController.login);
router.post('/register', UserController.register)
module.exports = router;
