const fs = require('fs')

class UserController {
    static findAll(req, res){
        fs.readFile('data.json', (err, data)=>{
            if(err){
                res.status(400).json({"errors": "error membaca data"})
            }else {
                let realData = JSON.parse(data)
                res.status(200).json({message: "berhasil get data", data: realData})
            }
        })
    }
    static register(req, res){
        fs.readFile('data.json', (err, data)=>{
            if(err){
                res.status(400).json({"errors": "error membaca data"})
            }else{
                let existingData = JSON.parse(data)
                // let { users } = existingData
                let {name, password, role} = req.body
                let newUser = {name, password, role, "isLogin": false}
                let newUser1 = {name, password, role, "isLogin": false, "students": []}
                // let newData = {...existingData, users}
                if(role=="admin"){
                    existingData.push(newUser)
                    fs.writeFile('data.json',JSON.stringify(existingData),(err)=>{
                        if(err){
                            res.status(400).json({errors: "error menyimpan data"})
                        }
                        else{
                            res.status(201).json({ message: "berhasil register" })
                        }
                    })
                }else if(role=="trainer"){
                    existingData.push(newUser1)
                    fs.writeFile('data.json',JSON.stringify(existingData),(err)=>{
                        if(err){
                            res.status(400).json({errors: "error menyimpan data"})
                        }
                        else{
                            res.status(201).json({ message: "berhasil register" })
                        }
                    })
                }
                
            }
        })
    }
    static login(req,res) {
        fs.readFile('data.json', (err, data)=>{
            if(err){
                res.status(400).json({"errors": "error membaca data"})
            }else{
                let existingData = JSON.parse(data)
                // let { users } = existingData
                let {name, password} = req.body
                for(let i=0;i<existingData.length;i++){
                    if(name==existingData[i].name){
                        if(password==existingData[i].password){
                            existingData[i].isLogin=true
                            fs.writeFile('data.json',JSON.stringify(existingData),(err)=>{
                                if(err){
                                    res.status(400).json({errors: "error menyimpan data"})
                                }
                                else{
                                    res.status(201).json({ message: "berhasil Login" })
                                }
                            })
                        }
                        else{
                            res.status(302).json({errors: "password salah atau data tidak ditemukan"})
                        }
                    }
                    else{
                    }
                }
                // let newUser = {name, password, role, "isLogin": 0}
                // existingData.push(newUser)
                // let newData = {...existingData, users}
            }
        })
    }
    static addStudents(req, res) {
        // console.log("nama trainer : ",req.params.name)
        // res.send(`hello ${req.params.name} user`)
        fs.readFile('data.json', (err, data)=>{
            if(err){
                res.status(400).json({"errors": "error membaca data"})
            }else{
                let existingData=JSON.parse(data)
                for(let i = 0;i<existingData.length;i++){
                    if(existingData[i].name==req.params.name){
                        if(existingData[i].isLogin==false){
                            res.status(302).json({ message: "anda belum login" })
                        }
                        else{
                        let student = {}
                        let {name, kelas} = req.body
                        student.name = name
                        student.class = kelas
                        for(let j = 0;j<existingData.length;j++){
                            if(existingData[j].role=="trainer"){
                                // res.status(301).json({ message: "data berhasil ditambahkan" })
                                existingData[j].students.push(student)
                                fs.writeFile('data.json',JSON.stringify(existingData),(err)=>{
                                    if(err){
                                        res.status(400).json({errors: "error menyimpan data"})
                                    }
                                    else{
                                        res.status(201).json({ message: "berhasil add siswa" })
                                    }
                                })
                            }
                        }
                    }
                    }
                    else{

                    }
                }
            }
        })
    }
}
module.exports = UserController