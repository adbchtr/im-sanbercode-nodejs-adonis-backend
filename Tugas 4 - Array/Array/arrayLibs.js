function range(startNum, finishNum){
    array=[]
    var start = parseInt(startNum)
    var finish = parseInt(finishNum)
    if(startNum<finish){
        for (var i = start; i<=finish; i++){
            array.push(i)
        }
        console.log(array)
        }
    else if(start>finish){
        for (var i = start;i>=finish;i--){
            array.push(i)
        }
        console.log(array)
    }
    }
function rangeWithStep(startNum, finishNum, step){
    array=[]
    var start = parseInt(startNum)
    var finish = parseInt(finishNum)
    var st = parseInt(step)
    if(startNum<finish){
        for (var i = start; i<=finish; i=i+st){
            array.push(i)
         }
        console.log(array)
    }
    else if(start>finish){
        for (var i = start;i>=finish;i=i-st){
            array.push(i)
        }
        console.log(array)
    }
}
function sum(startNum , finishNum = "1", step = "1"){
    array=[]
    var start = parseInt(startNum)
    var finish = parseInt(finishNum)
    var st = parseInt(step)
    if(startNum<finish){
        var hasil = 0
        for (var i = start; i<=finish; i=i+st){
            array.push(i)
            hasil = hasil + i
         }
        console.log(hasil)
    }
    else if(start>finish){
        var hasil = 0
        for (var i = start;i>=finish;i=i-st){
            array.push(i)
            hasil = hasil + i
        }
        console.log(hasil)
    }
}
function balikKata(str){  
    var reversed = "";      
     for (var i = str.length - 1; i >= 0; i--){         
       reversed += str[i];  
     }     
    console.log(reversed)
  }
function dataHandling(){
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
      ] 
      for(var i = 0;i<input.length;i++){
        console.log(`
        Nomor ID : ${input[i][0]}
        Nama Lengkap : ${input[i][1]}
        TTL : ${input[i][2]} ${input[i][3]}
        Hobi : ${input[i][4]} 
        `)
      }
}

    module.exports = {
        range: range,
        rangeWithStep: rangeWithStep,
        sum: sum,
        balikKata: balikKata,
        dataHandling: dataHandling
        }