var array = require('./arrayLibs.js')
var range = array.range
var rangeWithStep = array.rangeWithStep
var sum = array.sum
var balikKata = array.balikKata
var dataHandling = array.dataHandling
var args = process.argv

switch(args[2]){
    case "range":
        range(args[3], args[4])
    break;
    case "rangeWithStep":
        rangeWithStep(args[3], args[4], args[5])
    break;
    case "sum":
        sum(args[3], args[4], args[5])
    break;
    case "balikKata":
        balikKata(args[3])
    break;
    case "dataHandling":
        dataHandling()
    break;
    default:
        console.log("perintah salah")
        console.log(`
        INSTRUKSI
        node <perintah> <parameter>
        `)
    break;
}