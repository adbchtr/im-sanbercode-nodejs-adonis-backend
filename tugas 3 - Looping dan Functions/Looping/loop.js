function loop1(){
    var init = 0
    console.log("LOOPING PERTAMA")
    while(init<20){
        init=init+2
        console.log(init+" - I Love Coding")
    }
    console.log("LOOPING KEDUA")
    while(init>0){
        console.log(init+" - I Love Coding")
        init=init-2
    }
}
function loop2(){
    for(var init = 1; init<=20; init++){
        if(init%2!=0){
            if (init%3==0){
                console.log(init+" - I Love Coding")
            }else {
                console.log(init+" - Santai")
            }
        }
        else {
            console.log(init+" - Berkualitas")
        }
    }
}
function loop3(brs, tng){
    var baris = brs
    var output=""
    var tinggi = tng
    for (var i = 0; i < tinggi; i++) {
        for (var j = 0; j < baris; j++) {
            output+="#"
        }
        console.log(output);
    output = "";
    }
}
function loop4(row){
    var rows = row;
var output=""
for(var i=1;i<=rows;i++){
    for(var j=1;j<=i;j++){
        output+="*"
        
    }
    console.log(output);
    output = "";
    }
}
function loop5(init){
    for (var baris =0; baris < init ; baris ++) {
        var output = ""
        for (var kolom = 0; kolom < init; kolom ++){
            if (baris % 2 == 0) {
                if(kolom % 2 == 0){
                    output+= " "
                }
                else {
                    output +="#"
                }
            }
            else {
                if(kolom%2==0){
                    output+= "#"
                }
                else {
                    output+= " "
                }
            }
        }
        console.log(output);
    }
}
module.exports = {
    loop1: loop1,
    loop2: loop2,
    loop3: loop3,
    loop4: loop4,
    loop5: loop5
}