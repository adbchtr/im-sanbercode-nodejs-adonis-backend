import {
    readData,
    writeData,
    addSiswa
}
from './lib/fscallback'

import {
    loginData
} from './lib/fspromise'
const args = process.argv.slice(2)
const command = args[0]
switch(command){
    case "readData":
        readData()
        break;
    case "register":
        let data = args[1].split(",")
        // console.log(data)
        let obj = {}
        obj["name"] = data[0]
        obj["password"] = data[1]
        obj["role"] = data[2]
        obj["isLogin"] = false
        writeData(obj)
        break;
    case "login":
        let dataLogin = args[1].split(",")
        // console.log(dataLogin)
        loginData(dataLogin[0],dataLogin[1])
        break;
    case "addSiswa":
        let simpan = args[1].split(",")
        addSiswa(simpan[0],simpan[1])
        break;
    default:
        break;
}
