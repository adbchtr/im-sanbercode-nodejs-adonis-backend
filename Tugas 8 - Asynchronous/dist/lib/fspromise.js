"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loginData = void 0;

var _promises = _interopRequireDefault(require("fs/promises"));

require("core-js/stable");

require("regenerator-runtime/runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var path = 'data.json';

var loginData = function loginData(nama, password) {
  _promises["default"].readFile(path).then(function (data) {
    var realData = JSON.parse(data);
    var ganti = {};

    for (var i = 0; i < realData.length; i++) {
      if (realData[i].name == nama && realData[i].password == password) {
        ganti = realData[i];
        ganti.isLogin = true;
      } else {}
    }

    console.log(ganti);
    var indexCari = realData.findIndex(function (item) {
      return item.name == nama && item.password == password;
    });
    realData.splice(indexCari, 1, ganti);
    return _promises["default"].writeFile(path, JSON.stringify(realData));
  }).then(function () {
    return console.log("berhasil login");
  })["catch"](function (err) {
    console.log(error);
  });
};

exports.loginData = loginData;