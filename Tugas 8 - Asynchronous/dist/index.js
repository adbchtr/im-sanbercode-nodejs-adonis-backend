"use strict";

var _fscallback = require("./lib/fscallback");

var _fspromise = require("./lib/fspromise");

var args = process.argv.slice(2);
var command = args[0];

switch (command) {
  case "readData":
    (0, _fscallback.readData)();
    break;

  case "register":
    var data = args[1].split(","); // console.log(data)

    var obj = {};
    obj["name"] = data[0];
    obj["password"] = data[1];
    obj["role"] = data[2];
    obj["isLogin"] = false;
    (0, _fscallback.writeData)(obj);
    break;

  case "login":
    var dataLogin = args[1].split(","); // console.log(dataLogin)

    (0, _fspromise.loginData)(dataLogin[0], dataLogin[1]);
    break;

  case "addSiswa":
    var simpan = args[1].split(",");
    (0, _fscallback.addSiswa)(simpan[0], simpan[1]);
    break;

  default:
    break;
}