"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _kelas2 = _interopRequireDefault(require("./kelas.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Bootcamp = /*#__PURE__*/function () {
  function Bootcamp(name) {
    _classCallCheck(this, Bootcamp);

    this._name = name;
    this._classes = [];
  }

  _createClass(Bootcamp, [{
    key: "name",
    get: function get() {
      return this._name;
    },
    set: function set(newName) {
      this._name = newName;
    }
  }, {
    key: "classes",
    get: function get() {
      return this._classes;
    }
  }, {
    key: "createClass",
    value: function createClass(className, level, instructor) {
      var newKelas = new _kelas2["default"](className, level, instructor);

      this._classes.push(newKelas);
    }
  }, {
    key: "register",
    value: function register(kelas, student) {
      var kel = this._classes.find(function (c) {
        return c.name == kelas;
      });

      kel.addStudent(student);
    }
  }, {
    key: "runBatch",
    value: function runBatch() {
      for (var c = 0; c < this._classes.length; c++) {
        var _kelas = this._classes[c];

        for (var i = 1; i <= 4; i++) {
          _kelas.assignNilai();
        }

        console.log("Graduated from ".concat(_kelas.name, ": "), _kelas.graduate());
      }
    }
  }]);

  return Bootcamp;
}();

exports["default"] = Bootcamp;