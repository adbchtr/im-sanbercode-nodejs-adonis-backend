"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _student2 = _interopRequireDefault(require("./student"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var kelas = /*#__PURE__*/function () {
  function kelas(name, level, instructor) {
    _classCallCheck(this, kelas);

    this._name = name;
    this._students = [];
    this._level = level;
    this._instructor = instructor;
  }

  _createClass(kelas, [{
    key: "name",
    get: function get() {
      return this._name;
    },
    set: function set(newName) {
      this._name = newName;
    }
  }, {
    key: "level",
    get: function get() {
      return this._level;
    },
    set: function set(lev) {
      this._level = lev;
    }
  }, {
    key: "students",
    get: function get() {
      return this._students;
    }
  }, {
    key: "addStudent",
    value: function addStudent(student) {
      this._students.push(student);
    }
  }, {
    key: "generateNilai",
    value: function generateNilai() {
      return Math.ceil(Math.random() * (100 - 50) + 50);
    }
  }, {
    key: "assignNilai",
    value: function assignNilai() {
      var _this = this;

      this._students.map(function (student) {
        student.addScore(_this.generateNilai());
      });
    }
  }, {
    key: "calculatedMean",
    value: function calculatedMean(arrScores) {
      return Math.ceil(arrScores.reduce(function (a, b) {
        return a + b;
      }, 0) / 4);
    }
  }, {
    key: "graduate",
    value: function graduate() {
      var obj = {
        participant: [],
        completed: [],
        mastered: []
      };

      var _iterator = _createForOfIteratorHelper(this._students),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var _student = _step.value;
          var mean = this.calculatedMean(_student.scores);
          _student.finalScore = mean;

          if (mean < 60) {
            obj.participant.push(_student);
          } else if (mean < 85) {
            obj.completed.push(_student);
          } else if (mean > 85) {
            obj.mastered.push(_student);
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      return obj;
    }
  }]);

  return kelas;
}();

exports["default"] = kelas;