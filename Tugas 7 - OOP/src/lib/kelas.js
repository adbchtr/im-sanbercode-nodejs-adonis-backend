import student from './student'
export default class kelas {
    constructor(name, level, instructor){
        this._name = name
        this._students = []
        this._level = level
        this._instructor = instructor
    }
    get name(){
        return this._name
    }
    set name(newName){
        this._name = newName
    }
    get level(){
        return this._level
    }
    set level(lev){
        this._level = lev
    }
    get students(){
        return this._students
    }
    addStudent(student){
        this._students.push(student)
    }
    generateNilai(){
        return Math.ceil(Math.random()*(100-50)+50)
    }
    assignNilai(){
        this._students.map((student)=>{
            student.addScore(this.generateNilai())
        })
    }
    calculatedMean(arrScores){
        return Math.ceil(arrScores.reduce((a, b)=>a+b,0)/4)
    }
    graduate(){
        let obj = {
            participant : [],
            completed : [],
            mastered : []
        }
        for (const student of this._students){
            let mean = this.calculatedMean(student.scores)
            student.finalScore = mean
            if(mean < 60){
                obj.participant.push(student)
            }
            else if(mean<85){
                obj.completed.push(student)
            }
            else if(mean>85){
                obj.mastered.push(student)
            }
        }
        return obj
    }
}