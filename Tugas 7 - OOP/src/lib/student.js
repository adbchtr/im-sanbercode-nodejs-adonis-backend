export default class student{
    constructor(nama){
        this._nama=nama
        this._scores = []
        this._finalScore = 0
    }
    get finalScore(){
        return this._finalScore
    }
    set finalScore(score){
        this._finalScore = score
    }
    get scores(){
        return this._scores
    }
    addScore(score){
        this._scores.push(score)
    }
}