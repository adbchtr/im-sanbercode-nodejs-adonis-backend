// const kelas = require('./kelas')
import kelas from './kelas.js'
export default class Bootcamp {
    constructor(name){
        this._name = name
        this._classes = []
    }
    get name(){
        return this._name
    }
    set name(newName){
        this._name = newName
    }
    get classes(){
        return this._classes
    }
    createClass(className, level, instructor){
        let newKelas = new kelas(className, level, instructor)
        this._classes.push(newKelas)
    }
    register(kelas, student){
        let kel = this._classes.find(c=>c.name==kelas)
        kel.addStudent(student)
    }
    runBatch(){
        for(let c = 0;c<this._classes.length;c++){
            const kelas = this._classes[c]
            for(let i=1;i<=4;i++){
                kelas.assignNilai()
            }
            console.log(`Graduated from ${kelas.name}: `,kelas.graduate())
        }
    }
}