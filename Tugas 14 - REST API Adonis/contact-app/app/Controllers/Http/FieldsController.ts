import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateBookingValidator from 'App/Validators/CreateBookingValidator';
import Database from '@ioc:Adonis/Lucid/Database'
import Field from 'App/Models/Field';

export default class FieldsController {
  public async index({ response, request}:HttpContextContract){
    if(request.qs().name){
      let name = request.qs().name
      let fieldsFiltered = await Field.findBy("name", name)
      return response.status(200).json({message: 'success get field data', data:fieldsFiltered})
  }
  let fields = await Field.all()
  return response.status(200).json({message: 'success get all field data', data:fields})
}

  public async store({request, response}:HttpContextContract){
    try {
          await request.validate(CreateBookingValidator);
          let newField = new Field();
          newField.name = request.input('name')
          newField.type = request.input('type')
          newField.venue_id = request.input('venue_id')

          await newField.save()
          console.log("id : ",newField.id)
          response.created({message: 'created! '})
      } catch (error) {
          response.badRequest({errors:error.messages})
      }
    }

    public async show({params, response}:HttpContextContract){
      let field = await Field.find(params.id)
        return response.ok({message: 'success get field with id', data: field})
  }

  public async update({request, response, params}:HttpContextContract){
    let id = params.id
        let field = await Field.findOrFail(id)
        field.name = request.input('name')
        field.type = request.input('type')
        field.venue_id = request.input('venue_id')
        field.save()
        return response.ok({message: 'updated!'})
}

public async destroy({params, response}:HttpContextContract){
  let field = await Field.findOrFail(params.id)
        await field.delete()
        return response.ok({message:'deleted!'})
}
}
