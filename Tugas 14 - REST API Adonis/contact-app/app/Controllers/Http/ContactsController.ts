import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateContactValidator from 'App/Validators/CreateContactValidator';
import { cuid } from '@ioc:Adonis/Core/Helpers'
import Application from '@ioc:Adonis/Core/Application'
// import Database from '@ioc:Adonis/Lucid/Database'
import Contact from 'App/Models/Contact';

export default class ContactsController {
    public async index({ response, request }:HttpContextContract){
        // console.log(request.qs())
        if(request.qs().name){
            //query builder
            let name = request.qs().name
            // let contactsFiltered = await Database.from('contacts').select('*')
            // .where('name', name)
            //model ORM
            let contactsFiltered = await Contact.findBy("name", name)
            return response.status(200).json({message: 'success get contact', data:contactsFiltered})
            }
        //Query builder
            // let contacts = await Database.from('contacts').select('*')
        //Model ORM
            let contacts = await Contact.all()
            return response.status(200).json({message: 'success get contact', data:contacts})
    }

    public async store({request, response, auth}:HttpContextContract){
        try {
            const contact = await request.validate(CreateContactValidator);
            // query builder
            // let newUserId = await Database.table('contacts').returning('id').insert({
            //     name: request.input('name'),
            //     email: request.input('email'),
            //     phone: request.input('phone')
            // })
            // model ORM
            const fileName = `${contact.thumbnail.clientName}`
            // console.log(fileName)
            let uploaded = await contact.thumbnail.move(Application.tmpPath('uploads'), {
                name: fileName,
            })
            console.log(contact.thumbnail.fileName)
            // console.log(uploaded)
              const newContact = await Contact.create({name:contact.name, email:contact.email, phone:contact.phone,thumbnail:`/uploads/${contact.thumbnail.fileName}` })
            return response.created({message: 'created! '})
        } catch (error) {
            return response.unprocessableEntity({errors:error.messages})
        }
    }
    public async show({params, response}:HttpContextContract){
        //Query Builder
        // let contact = await Database.from('contacts').where('id', params.id).select('*').firstOrFail()
        //Model ORM
        let contact = await Contact.find(params.id)
        return response.ok({message: 'success get contact with id', data: contact})
    }
    public async update({request, response, params}:HttpContextContract){
        let id = params.id
        //query builder
        // await Database.from('contacts').where('id', id)
        // .update({
        //     email: request.input('email'),
        //     name: request.input('name'),
        //     phone: request.input('phone')
        // })
        //model ORM
        let contact = await Contact.findOrFail(id)
        contact.name = request.input('name')
        contact.email = request.input('email')
        contact.phone = request.input('phone')
        contact.save()
        return response.ok({message: 'updated!'})
    }
    public async destroy({params, response}:HttpContextContract){
        //query builder
        // await Database.from('contacts')
        // .where('id', params.id)
        // .delete()

        //model ORM
        let contact = await Contact.findOrFail(params.id)
        await contact.delete()
        return response.ok({message:'deleted!'})
    }
}
