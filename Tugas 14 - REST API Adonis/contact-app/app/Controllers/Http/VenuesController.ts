import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
// import CreateVenueValidator from 'App/Validators/CreateVenueValidator'
// import Database from '@ioc:Adonis/Lucid/Database'
import Venue from 'App/Models/Venue'

export default class VenuesController {
    public async index({ response, request}:HttpContextContract){
        if(request.qs().name){
            let name = request.qs().name
            let venuesFiltered = await Venue.findBy("name", name)
            return response.status(200).json({message: 'success get venue data', data:venuesFiltered})
        }
        let venues = await Venue.all()
        return response.status(200).json({message: 'success get all venues data', data:venues})
    }
    public async show({params, response}:HttpContextContract){
        let venue = await Venue.find(params.id)
        return response.ok({message: 'success get venue with id', data: venue})
    }
    public async store({request, response}:HttpContextContract){
        try {
            let newVenue = new Venue();
            newVenue.name = request.input('name')
            newVenue.address = request.input('address')
            newVenue.phone = request.input('phone')

            await newVenue.save()
            console.log("id : ",newVenue.id)
            response.created({message: 'created! '})
        } catch (error) {
            response.badRequest({errors:error.messages})
        }
    }
    public async update({request, response, params}:HttpContextContract){
        let id = params.id
        let venue = await Venue.findOrFail(id)
        venue.name = request.input('name')
        venue.address = request.input('address')
        venue.phone = request.input('phone')
        venue.save()
        return response.ok({message: 'updated!'})
    }
    public async destroy({params, response}:HttpContextContract){
        let venue = await Venue.findOrFail(params.id)
        await venue.delete()
        return response.ok({message:'deleted!'})
    }
}
