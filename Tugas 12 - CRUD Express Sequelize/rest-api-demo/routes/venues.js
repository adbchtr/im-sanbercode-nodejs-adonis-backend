var express = require('express');
var router = express.Router();
const asyncMiddleware = require('../middlewares/asyncMiddleware')
const VenueController = require('../controllers/venues');
const VenuesController = require('../controllers/venues');
router.post('/', asyncMiddleware(VenueController.save))
router.get('/', asyncMiddleware(VenuesController.findAll))
router.get('/:id', asyncMiddleware(VenueController.show))
router.post('/delete', asyncMiddleware(VenueController.delete))
module.exports=router