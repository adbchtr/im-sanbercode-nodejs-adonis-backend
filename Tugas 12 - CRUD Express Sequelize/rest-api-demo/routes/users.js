var express = require('express');
var router = express.Router();
//controller
const UserController = require('../controllers/users')
/* GET users listing. */
router.get('/', UserController.findAll);
router.get('/:id',(req, res)=>{
  console.log("parameter : ", req.params)
  res.send(`hello ${req.params.id} user`)
})
router.post('/', UserController.register)

module.exports = router;
