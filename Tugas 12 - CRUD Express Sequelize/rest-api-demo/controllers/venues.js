const Venue = require('../models/venue')

class VenuesController{
    static async save(req, res, next){
        // let newVenue = Venue.build({name:'Andra', address: 'cikunir',phone:'14045'})
        // console.log("cek ", newVenue instanceof Venue)
        // console.log(newVenue.name)
        // await newVenue.save()
        let {name, address, phone} = req.body
        const venue = await Venue.create({name, address, phone})
        res.status(201).json({message:'created', data:venue})
    }
    static async findAll(req, res, next){
        let venues = await Venue.findAll({
            attributes: ['name', 'address', 'phone']
        })
        res.status(200).json({message: 'success', venues})
    }
    static async show(req, res, next){
        let {id} = req.params
        let venue = await Venue.findOne({where: 
            {id:id}
        })
        res.status(200).json({message: 'Success', venue})
    }
    static async delete(req, res,next){
        let {id} = req.body
        const venue = await Venue.drop({where:
        {id:id}
    })
    res.status(200).json({message: 'Success', venue})
    }
}
module.exports = VenuesController