import { hello, 
    dataDiri,
    checkScore,
    filterData} from './lib/eslib'
const args = process.argv
switch (args[2]){
case "sapa":
    console.log(hello(args[3]))
    break;
case "convert":
    dataDiri(args[3],args[4],args[5])
    break;
case "checkScore":
    console.log(checkScore(args[3]))
    break;
case "filterData":
    filterData(args[3])
    break;
}