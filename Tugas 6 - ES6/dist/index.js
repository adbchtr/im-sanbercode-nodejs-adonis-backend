"use strict";

var _eslib = require("./lib/eslib");

var args = process.argv;

switch (args[2]) {
  case "sapa":
    console.log((0, _eslib.hello)(args[3]));
    break;

  case "convert":
    (0, _eslib.dataDiri)(args[3], args[4], args[5]);
    break;

  case "checkScore":
    console.log((0, _eslib.checkScore)(args[3]));
    break;

  case "filterData":
    (0, _eslib.filterData)(args[3]);
    break;
}