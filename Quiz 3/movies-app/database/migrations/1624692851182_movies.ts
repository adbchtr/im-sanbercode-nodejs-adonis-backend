import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Movies extends BaseSchema {
  protected tableName = 'movies'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.string('title'),
      table.text('resume'),
      table.integer('genre_id')
      .unsigned()
      .references('genres.id')
      .onDelete('CASCADE')
      table.dateTime('release_date')
      table.timestamps(true, true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
