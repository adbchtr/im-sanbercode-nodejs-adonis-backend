import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
export default class MoviesController {
  public async index ({ response}: HttpContextContract) {
    let fields = await Database.from('movies')
    .join('genres', 'movies.genre_id', '=', 'genres.id')
    .select({
      id:'movies.id',
      title:'movies.title',
      resume:'movies.resume',
      release_date:'movies.release_date'
    })
    .select('genres.name as genre')
        return response.status(200).json({message: 'success get movies', data:fields})
  }
  public async store({request, response}:HttpContextContract){
    try {
        // await request.validate(CreateVenueValidator);
        // console.log(request.all())
        let newUserId = await Database.table('movies').returning('id').insert({
            title: request.input('title'),
            resume: request.input('resume'),
            release_date: request.input('release_date'),
            genre_id: request.input('genre_id'),
            })
        response.created({message: 'created! ', newId:newUserId})
    } catch (error) {
        response.badRequest({errors:error.messages})
    }
}
  public async show({params, response}:HttpContextContract){
    let venue = await Database.from('movies').join('genres', 'movies.genre_id', '=', 'genres.id')
    .select({
      id:'movies.id',
      title:'movies.title',
      resume:'movies.resume',
      release_date:'movies.release_date'
    })
    .select('genres.name as genre').where('movies.id', params.id).firstOrFail()
    return response.ok({message: 'success get movies with id', data: venue})
}
  public async update({request, response, params}:HttpContextContract){
    let id = params.id
    await Database.from('movies').where('id', id)
    .update({
      title: request.input('title'),
      resume: request.input('resume'),
      release_date: request.input('release_date'),
      genre_id: request.input('genre_id'),
        })
    return response.ok({message: 'updated!'})
}
public async destroy({params, response}:HttpContextContract){
  await Database.from('movies')
  .where('id', params.id)
  .delete()
  return response.ok({message:'deleted!'})
}
}
