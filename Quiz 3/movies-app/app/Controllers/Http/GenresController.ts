import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'

export default class GenresController {
  public async index ({ response}: HttpContextContract) {
    let fields = await Database.from('genres').select('*')
        return response.status(200).json({message: 'success get genres', data:fields})
  }
  public async store({request, response}:HttpContextContract){
    try {
        // await request.validate(CreateVenueValidator);
        // console.log(request.all())
        let newUserId = await Database.table('genres').returning('id').insert({
            name: request.input('name'),
            })
        response.created({message: 'created! ', newId:newUserId})
    } catch (error) {
        response.badRequest({errors:error.messages})
    }
}
  public async show({params, response}:HttpContextContract){
    let genre = await Database.from('genres').select({
      id:'id',
      name:'name'
    }).where('id', params.id)
    let venue = await Database.from('movies').join('genres', 'movies.genre_id', '=', 'genres.id')
    .where('movies.genre_id', params.id).select({id:'movies.id',
                                                title:'movies.title',
                                                release_data:'release_date',
                                                resume:'resume'})
    return response.ok({message: 'success get genre with id',genre, movies: venue})
}
  public async update({request, response, params}:HttpContextContract){
    let id = params.id
    await Database.from('genres').where('id', id)
    .update({
        name: request.input('name'),
        })
    return response.ok({message: 'updated!'})
}
public async destroy({params, response}:HttpContextContract){
  await Database.from('genres')
  .where('id', params.id)
  .delete()
  return response.ok({message:'deleted!'})
}
}