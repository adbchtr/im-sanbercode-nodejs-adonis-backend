import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Venue from 'App/Models/Venue'
import Book from 'App/Models/Book'
import PlayerBook from 'App/Models/PlayerBook'
import Database from '@ioc:Adonis/Lucid/Database'
import CreateVenueValidator from 'App/Validators/CreateVenueValidator'
export default class VenuesController {
  /**
   * @swagger
   * /venues:
   *    get:
   *      security:
   *        - bearerAuth: []
   *      tags:
   *        - Venue
   *      summary: Get all venues data
   *      responses:
   *        200:
   *          description: success get venue
   *          content:
   *            application/json:
   *              schemas:
   *                message:
   *                  type: string
   *                  example:
   *                    message: success fetch venues
   *                  data:
   *                    type: array
   *                    items:
   *                      type: object
   *                      properties:
   *                        id: integer
   *                        name: string
   *                    example:
   *                      - id: 1
   *                        name: SMAN 1 KAB. TANGERANG
   */
  public async index ({request, response, auth}: HttpContextContract) {
    if (request.qs().name) {
      let name = request.qs().name
      let venuesFiltered = await Venue.findBy("name", name)
      return response.status(200).json({message: 'success get venue', data:venuesFiltered})
    } else {
      let venues = await Venue.all()
      return response.status(200).json({message: 'success get venue', data:venues})
    }
  }
/**
 * 
 * @swagger
 * /venues:
 *  post:
 *    security:
*        - bearerAuth: []
 *    tags:
 *      - Venue
 *    requestBody:
 *      required: true
 *      content:
 *        application/x-www-form-urlencoded:
 *          schema:
 *            $ref: '#definitions/Venue'
 *        application/json:
 *          schema:
 *            $ref: '#definitions/Venue' 
 *    responses:
 *      '201':
 *        description: Venue created
 *      '422':
 *        description: Unprocessable Entity
 */
  public async store ({request, response}: HttpContextContract) {
    try {
      const venue = await request.validate(CreateVenueValidator);
      const newVenue = await Venue.create(venue)
      return response.created({message: 'created! ', data: newVenue})
    } catch (error) {
      return response.unprocessableEntity({errors:error.messages})
    }
  }
/**
 * @swagger
 * /venues/{id}:
 *    get:
 *      security:
 *        - bearerAuth: []
 *      tags:
 *        - Venue
 *      summary: Returns a venue by ID
 *      parameters:
 *        - name: id
 *          in: path
 *          required: true
 *          description: the ID of the venue to return
 *          schema:
 *            type: integer
 *            format: int64
 *            minimum: 1
 *      responses:
 *        '200':
 *          description: a venue object.
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    format: int64
 *                    example: 2
 *                  name:
 *                    type: string
 *                    example: SMAN 1 Kab.Tangerang
 *                  address:
 *                    type: string
 *                    example: jl.raya serang km.22
 *                  phone:
 *                    type: string
 *                    example: 083812146223
 *                  created_at:
 *                    type: datetime
 *                    example: 2021-06-30T00:07:58.000+07:00
 *                  updated_at:
 *                    type: datetime
 *                    example: 2021-06-30T00:07:58.000+07:00
 *                  fields:
 *                    type: array of object
 *                    example: [{id: 1,
 *                              name: 'abdul hamid',
 *                              type: 'sepakbola',
 *                              venue_id: 2,
 *                              created_at: 2021-06-30T00:07:58.000+07:00,
 *                              update_at: 2021-06-30T00:07:58.000+07:00}]
 *        '400':
 *          description: the specified venue id is invalid (not a number)
 *        default:
 *          description: unexpected error
 */
  public async show ({params, response}: HttpContextContract) {
    const venue = await Venue.query().preload('fields').where('id', params.id).first()
    return response.ok({message: 'success get venue with id', data: venue})
  }

/**
 * 
 * @swagger
 * /venues/{id}:
 *  put:
 *    security:
 *        - bearerAuth: []
 *    tags:
 *      - Venue
 *    parameters:
 *      - name: id
 *        in: path
 *        required: true
 *        description: the ID of the venue to update
 *        schema:
 *          type: integer
 *          format: int64
 *          minimum: 1
 *    requestBody:
 *      required: true
 *      content:
 *        application/x-www-form-urlencoded:
 *          schema:
 *            $ref: '#definitions/Venue'
 *        application/json:
 *          schema:
 *            $ref: '#definitions/Venue' 
 *    responses:
 *      '201':
 *        description: Venue edited
 *      '422':
 *        description: Unprocessable Entity
 */
  public async update ({request, response, params}: HttpContextContract) {
    let id = params.id
        let user = await Venue.findOrFail(id)
        user.name = request.input('name')
        user.address = request.input('address')
        user.phone = request.input('phone')
        user.save()
        return response.ok({message: 'updated!'})
  }
/**
 * @swagger
 * /venues/{id}:
 *    delete:
 *      security:
 *        - bearerAuth: []
 *      tags:
 *        - Venue
 *      summary: delete a venue by ID
 *      parameters:
 *        - name: id
 *          in: path
 *          required: true
 *          description: the ID of the venue to delete
 *          schema:
 *            type: integer
 *            format: int64
 *            minimum: 1
 *      responses:
 *        '200':
 *          description: deleted
 *        default:
 *          description: unexpected error
 */
  public async destroy ({params, response}: HttpContextContract) {
    let venue = await Venue.findOrFail(params.id)
        await venue.delete()
        return response.ok({message:'deleted!'})
  }
/**
 * 
 * @swagger
 * /venues/{id}/bookings:
 *  post:
 *    security:
 *        - bearerAuth: []
 *    tags:
 *      - Booking
 *    summary: create a booking based on a field by ID
 *    parameters:
 *      - name: id
 *        in: path
 *        required: true
 *        description: the ID of the field to book
 *        schema:
 *          type: integer
 *          format: int64
 *          minimum: 1
 *    requestBody:
 *      required: true
 *      content:
 *        application/x-www-form-urlencoded:
 *          schema:
 *            type: object
 *            properties:
 *              play_date_start:
 *                type: datetime
 *              play_date_end:
 *                type: datetime
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              play_date_start:
 *                type: datetime
 *              play_date_end:
 *                type: datetime
 *    responses:
 *      '201':
 *        description: Venue created
 *      '422':
 *        description: Unprocessable Entity
 */
  public async book({params, response, request, auth}: HttpContextContract){
    try {
      const book = new Book()
      book.fieldId = params.id
      book.playDateStart = request.input('play_date_start')
      book.playDateEnd = request.input('play_date_end')
      const authUser = auth.user
      await authUser?.related('books').save(book)
      return response.created({message: 'Books created' })
    } catch (error) {
      return response.unprocessableEntity({message: error.messages})
    }
  }
/**
 * @swagger
 * /bookings/{id}:
 *    get:
 *      security:
 *        - bearerAuth: []
 *      tags:
 *        - Booking
 *      summary: Returns a bookings by ID
 *      parameters:
 *        - name: id
 *          in: path
 *          required: true
 *          description: the ID of the bookings to return
 *          schema:
 *            type: integer
 *            format: int64
 *            minimum: 1
 *      responses:
 *        '200':
 *          description: success get Book with id.
 *        '400':
 *          description: the specified venue id is invalid (not a number)
 *        default:
 *          description: unexpected error
 */
  public async getbook({params, response}:HttpContextContract){
    const book = await Book.query().preload('players',  (profileQuery) => {
      profileQuery.select({
        name: 'name',
        email: 'email'
      })
    }).where('id', params.id).first()
        return response.ok({message: 'success get Book with id', data: book})
  }

  /**
   * @swagger
   * /bookings:
   *    get:
   *      security:
   *        - bearerAuth: []
   *      tags:
   *        - Booking
   *      summary: Get all bookings data
   *      responses:
   *        200:
   *          description: success get book
   */
  public async allbook({response}:HttpContextContract){
    const book = await Book.query().preload('players',  (profileQuery) => {
      profileQuery.select({
        name: 'name',
        email: 'email'
      })
    })
        return response.ok({message: 'success get Book', data: book})
  }
/**
 * 
 * @swagger
 * /bookings/{id}/join:
 *  post:
 *    security:
 *        - bearerAuth: []
 *    tags:
 *      - Booking
 *    summary: join a booking based on a user by ID
 *    parameters:
 *      - name: id
 *        in: path
 *        required: true
 *        description: the ID of the booking to book
 *        schema:
 *          type: integer
 *          format: int64
 *          minimum: 1
 *    responses:
 *      '201':
 *        description: Venue created
 *      '422':
 *        description: Unprocessable Entity
 */
  public async addbook({params, response, auth}:HttpContextContract){
    try {
        const play = new PlayerBook()
        play.bookId = params.id
        const authUser = auth.user
      await authUser?.related('players').save(play)
      return response.created({message: 'Books joined' })
    } catch (error) {
      return response.unprocessableEntity({message: error.messages})
    }
  }

  /**
 * @swagger
 * /bookings/{id}/unjoin:
 *    delete:
 *      security:
 *        - bearerAuth: []
 *      tags:
 *        - Booking
 *      summary: delete a booking by ID and user ID
 *      parameters:
 *        - name: id
 *          in: path
 *          required: true
 *          description: the ID of the book_id to delete
 *          schema:
 *            type: integer
 *            format: int64
 *            minimum: 1
 *      responses:
 *        '200':
 *          description: deleted
 *        default:
 *          description: unexpected error
 */
  public async deletebook({params, response, auth}:HttpContextContract){
    await PlayerBook.query().where('book_id', params.id).where('user_id', auth.user?.$attributes.id ).delete()
    // let playerBook = getdata.find(auth.user?.$attributes.id)
    return response.ok({message:'deleted!'})
  }
  public async getbookbyuser({response}:HttpContextContract){
   const book = await Database
      .from('books')
      .join('users', 'books.booking_user_id', '=', 'users.id')
      .select('books.*')
      .where('users.loggedIn',1)
        return response.ok({message: 'success get Book', data: book})
}
}
