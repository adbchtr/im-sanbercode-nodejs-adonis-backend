import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateUserValidator from 'App/Validators/CreateUserValidator'
import User from 'App/Models/User'
import { schema } from '@ioc:Adonis/Core/Validator'
import Database from '@ioc:Adonis/Lucid/Database'
import Mail from '@ioc:Adonis/Addons/Mail'

export default class AuthController {
/**
 * 
 * @swagger
 * /register:
 *  post:
 *    tags:
 *      - Authentication
 *    requestBody:
 *      required: true
 *      content:
 *        application/x-www-form-urlencoded:
 *          schema:
 *            $ref: '#definitions/User'
 *        application/json:
 *          schema:
 *            $ref: '#definitions/User' 
 *    responses:
 *      '201':
 *        description: user created, verify otp in email
 *      '422':
 *        description: request invalid
 */
    public async register({request, response}:HttpContextContract){
        try {
            const data = await request.validate(CreateUserValidator)
            const newUser = await User.create(data)
            const otp_code = Math.floor(100000 + Math.random() * 900000);
            let saveCode = await Database.table('otp_codes').insert({
                otp_code: otp_code, user_id:newUser.id
            })
            await Mail.send((message) => {
                message
                  .from('admin@myapi.com')
                  .to(data.email)
                  .subject('Welcome Onboard!')
                  .htmlView('emails/otp_verification', {otp_code})
              })

            return response.created({message:'registered! please verify otp code', input:newUser})
        } catch (error) {
            console.log(error)
            return response.unprocessableEntity({message:error.messages})
        }
    }
/**
 * 
 * @swagger
 * /login:
 *  post:
 *    tags:
 *      - Authentication
 *    requestBody:
 *      required: true
 *      content:
 *        application/x-www-form-urlencoded:
 *          schema:
 *            $ref: '#definitions/User_login'
 *        application/json:
 *          schema:
 *            $ref: '#definitions/User_login' 
 *    responses:
 *      '201':
 *        description: login success   
 */
    public async login({request, response, auth}: HttpContextContract){
        try {
            const userSchema = schema.create({
                email: schema.string(),
                password: schema.string()
            })
            await request.validate({schema:userSchema})
            const email = request.input('email')
            const password = request.input('password')
            const token = await auth.use('api').attempt(email, password)
            await User
                .query()
                .where('email', email)
                .update({ loggedIn: 1 })
            return response.ok({message: 'login success', token})
        } catch (error) {
            if (error.guard) {
                return response.badRequest({message:'login error', error: error.message})
            } else {
                return response.badRequest({message:'login error', error: error.messages})
            }
        }
    }
/**
 * 
 * @swagger
 * /verifikasi-otp:
 *  post:
 *    tags:
 *      - Authentication
 *    requestBody:
 *      required: true
 *      content:
 *        application/x-www-form-urlencoded:
 *          schema:
 *            type: object
 *            properties:
 *              otp_code:
 *                type: string
 *              email:
 *                type: string
 *            required:
 *              - otp_code
 *              - email
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              otp_code:
 *                type: string
 *              email:
 *                type: string
 *              required:
 *              - otp_code
 *              - email 
 *    responses:
 *      '201':
 *        description: otp verification success   
 */
    public async otpConfirmation({request, response}:HttpContextContract){
        let otp_code = request.input('otp_code')
        let email = request.input('email')

        let user = await User.findBy('email', email)
        let otpCheck = await Database.query().from('otp_codes').where('otp_code',otp_code).first()

        if (user?.id==otpCheck.user_id) {
            user.isVerified = true
            await user?.save()
            return response.status(200).json({message: 'berhasil konfirmasi OTP'})
        } else {
            return response.status(400).json({message: 'gagal verifikasi OTP'})
        }
    }
}
