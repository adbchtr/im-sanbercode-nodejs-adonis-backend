import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class PlayerBook extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  bookId: number
  @column()
  userId: number
}
