import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class All {
  public async handle ({auth, response}: HttpContextContract, next: () => Promise<void>) {
    let isVerified = auth.user?.roleId
    if (isVerified==1) {
      await next()
    }
    else if(isVerified==2){
      await next()
    }
    else {
      await next()
    }
  }
}
