import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class Permission {
  public async handle ({auth, response}: HttpContextContract, next: () => Promise<void>) {
    let level = auth.user?.roleId
    if (level==1) {
      await next()
    }
    else if(level==2){
      await next()
    }
    else {
      return response.unauthorized({message: 'Aku tidak mendapat akses'})
    }
  }
}
