import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class User {
  public async handle ({auth, response}: HttpContextContract, next: () => Promise<void>) {
    let isVerified = auth.user?.roleId
    if (isVerified==3) {
      await next()
    } else {
      return response.unauthorized({message: 'anda bukan user'})
    }
  }
}
