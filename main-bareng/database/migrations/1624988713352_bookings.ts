import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Bookings extends BaseSchema {
  protected tableName = 'books'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.integer('field_id')
      .unsigned()
      .references('fields.id')
      .onDelete('CASCADE')
      table.dateTime('play_date_start')
      table.dateTime('play_date_end')
      table.integer('booking_user_id')
      .unsigned()
      .references('users.id')
      .onDelete('CASCADE')
      table.timestamps(true, true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
